package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.Scene;

public interface View {

    Scene getScene();

    //GameController getController();

    //EventHandler<KeyEvent> getKeyHandler();
}
