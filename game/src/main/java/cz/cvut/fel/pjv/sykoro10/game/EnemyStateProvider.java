package cz.cvut.fel.pjv.sykoro10.game;

import java.util.HashMap;

public class EnemyStateProvider {
    private HashMap<EnemyState.Type, EnemyState> pool = new HashMap<>();

    public  EnemyState getEnemyState(EnemyState.Type type){
        if (pool.containsKey(type)){
            return pool.get(type);
        }
        EnemyState enemyState = new EnemyState(type);
        pool.put(type, enemyState);
        return enemyState;
    }
}
