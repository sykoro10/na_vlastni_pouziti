package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.canvas.GraphicsContext;

public interface Character {


    void updatePos(double x, double y);
    void draw(GraphicsContext graphicsContext);
    Object getBoundary();
    default boolean overlaps(Character character) {
        return this.getBoundary().overlaps(character.getBoundary());
    }
    void attack(Character[] target);

    int getHp();
    void onHit();

    boolean isAttacking();
}
