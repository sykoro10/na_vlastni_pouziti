package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.image.Image;

public class EnemyState {
    private Image image;
    public EnemyState(EnemyState.Type type){
        setImage(type);
    }

    public void setImage(EnemyState.Type type){
        String path = null;
        switch (type){
            case Idle -> path = "/org/Colour1/Outline/120x80_gifs/__Idle.gif";
            case Run -> path = "/org/Colour1/Outline/120x80_gifs/__Run.gif";
        }
        if (path == null){
            throw new IllegalStateException("Path does not exist");
        }
        this.image = new Image(path);
    }

    public Image getImage(){
        return image;
    }

    public enum Type {
        Idle("/org/Colour1/Outline/120x80_gifs/__Idle.gif"),
        Run("/org/Colour1/Outline/120x80_gifs/__Run.gif");

        final  String resPath;

        Type(String resPath){
            this.resPath = resPath;
        }
    }
}
