package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.canvas.GraphicsContext;

public class Game {

    private GraphicsContext graphicsContext;
    double canvasWidth;
    double canvasHeight;
    private Player player;
    private Enemy enemy;

    public void initialize(){
        this.player = new Player(50, 50, 5);
        setPlayerState(Sprite.Type.Idle);
        this.enemy = new Enemy(5, 5, 2);
        setEnemyState(Sprite.Type.Idle);
    }

    public void setGraphicsContext(GraphicsContext graphicsContext, double canvasWidth, double canvasHeight){
        this.graphicsContext = graphicsContext;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
    }

    public void setPlayerState(Sprite.Type type){
        player.setState(type);
    }

    public void setEnemyState(Sprite.Type type){
        enemy.setState(type);
    }

    public void playerAttack(){
        Character[] enemies = {enemy};
        if (player.overlaps(enemy)){
            player.attack(enemies);
        } else {
            player.setAttackAnimation();
        }
    }

    public void updatePlayer(double dx, double dy){
        if ((dx == 0 && dy == 0)){
            return;
        }
        player.updatePos(dx, dy);
    }

    public void updateEnemy(){
        if (enemy.overlaps(player)) {
            enemy.attack(new Character[]{player});
        } else {
            if (enemy.updatePos(player)) {
                enemy.setState(Sprite.Type.Run);
            } else {
                enemy.setState(Sprite.Type.Idle);

            }
        }
    }

    public int getPlayerLives(){
        return player.getHp();
    }

    public int getEnemyLives(){
        return enemy.getHp();
    }

    public void update(double playerDx, double playerDy){
        updatePlayer(playerDx, playerDy);
        if (enemy.getHp() > 0) {
            updateEnemy();
        }
    }
    public void render(){
        graphicsContext.clearRect(0, 0,canvasWidth, canvasHeight);
        player.draw(graphicsContext);
        if (enemy.getHp() > 0) {
            enemy.draw(graphicsContext);
        }
    }
}
