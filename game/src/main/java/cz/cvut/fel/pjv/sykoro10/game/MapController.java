package cz.cvut.fel.pjv.sykoro10.game;


import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MapController implements Initializable {


    @FXML
    private Canvas canvas;
    GraphicsContext context;
    Game game;
    private boolean up, down, left, right, attack;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.context = canvas.getGraphicsContext2D();
        this.game = new Game();
        game.initialize();
        game.setGraphicsContext(context, canvas.getWidth(), canvas.getHeight());


        AnimationTimer animationTimer = new AnimationTimer() {
            private long lastUpdate = 0;
            @Override
            public void handle(long l) {
                if (l - lastUpdate >= 2000000) {
                    int dx = 0, dy = 0;
                    if (up) dy -= 2;
                    if (down) dy += 2;
                    if (left) dx -= 2;
                    if (right) dx += 2;
                    if (attack) game.playerAttack();
                    game.update(dx, dy);
                    game.render();
                    if (game.getPlayerLives() <= 0){
                        stop();
                    }
                    lastUpdate = l;
                }
            }
        };
        animationTimer.start();
    }

    public void setEventHandler(Scene scene){
        scene.setOnKeyPressed(keyEvent -> {
            switch (keyEvent.getCode()){
                case W:
                    System.out.println("w");
                    up = true;
                    break;
                case A:
                    left = true;
                    break;
                case S:
                    down = true;
                    break;
                case D:
                    right = true;
                    break;
                case SPACE:
                    attack = true;
                    break;
            }
        });

        scene.setOnKeyReleased(keyEvent -> {
            switch (keyEvent.getCode()){
                case W:
                    up = false;
                    break;
                case A:
                    left = false;
                    break;
                case S:
                    down = false;
                    break;
                case D:
                    right = false;
                    break;
                case SPACE:
                    attack = false;
                    break;

            }
        });
    }
}
