package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Enemy extends AbstractCharacter{

    public Enemy(double x,double y, int lives) {
        super(x, y, lives);
    }

    public boolean updatePos(Player player) {
        if (isAttacking()) {
            return false;
        }
        double currX = 0, currY = 0;
        if (player.getPosX() < this.getPosX()) {
            currX -= 1;
        } else if (player.getPosX() > this.getPosX()) {
            currX += 1;
        }
        if (player.getPosY() < this.getPosY()) {
            currY -= 1;
        } else if (player.getPosY() > this.getPosY()) {
            currY += 1;
        }
        updatePos(currX, currY);
        return currX > 0 || currY > 0;
    }
}
