package cz.cvut.fel.pjv.sykoro10.game;

public class Object {
    public double x;
    public double y;
    public double width;
    public double height;

    public Object(double x, double y, double width, double height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public boolean overlaps(Object other){
        boolean noOverlap = this.x + this.width < other.x || this.x > other.x + other.width ||
                this.y + this.height < other.y || this.y > other.y + other.height;
        return !noOverlap;
    }
}
