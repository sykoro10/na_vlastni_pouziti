package cz.cvut.fel.pjv.sykoro10.game;

import java.util.HashMap;

public class PlayerStateProvider {
    private HashMap<PlayerState.Type, PlayerState> pool = new HashMap<>();

    public  PlayerState getPlayerState(PlayerState.Type type){
        if (pool.containsKey(type)){
            return pool.get(type);
        }
        PlayerState playerState = new PlayerState(type);
        pool.put(type, playerState);
        return playerState;
    }
}
