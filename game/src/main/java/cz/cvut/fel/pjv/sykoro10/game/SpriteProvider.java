package cz.cvut.fel.pjv.sykoro10.game;

import java.util.HashMap;

public class SpriteProvider {
    private HashMap<Sprite.Type, Sprite> pool = new HashMap<>();

    public Sprite getPlayerState(Sprite.Type type){
        if (pool.containsKey(type)){
            return pool.get(type);
        }
        Sprite sprite = new Sprite(type);
        pool.put(type, sprite);
        return sprite;
    }
}
