package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.image.Image;


public class Sprite {
    private Image image;
    private Type type;
    public Sprite(Type type){
        this.type = type;
        setImage(type);
    }

    public Sprite.Type getType() {
        return type;
    }
    public void setImage(Type type){
        String path = type.resPath;
        if (path == null){
            throw new IllegalStateException("Path does not exist");
        }
        this.image = new Image(path);
    }

    public Image getImage() {
        return image;
    }

    public enum Type {
        Idle("/org/Colour2/Outline/120x80_gifs/__Idle.gif"),
        Attack("/org/Colour2/Outline/120x80_gifs/__Attack.gif"),
        Run("/org/Colour2/Outline/120x80_gifs/__Run.gif");

        final  String resPath;

        Type(String resPath){
            this.resPath = resPath;
        }
    }
}
