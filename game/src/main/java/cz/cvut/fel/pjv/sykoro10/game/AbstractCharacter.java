package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.canvas.GraphicsContext;

public abstract class AbstractCharacter implements Character{
    private double posX, posY;
    private final double width, height;
    private final double maxWidth, maxHeight;
    private Object boundary;
    private Sprite state;
    private final SpriteProvider spriteProvider = new SpriteProvider();
    private final long ATTACK_DELAY = 300L;
    private boolean isAttacking = false;
    private int lives;

    public AbstractCharacter(double x, double y, int lives) {
        this.posX = x;
        this.posY = y;
        this.width = 120;
        this.height = 80;
        this.boundary = new Object(posX, posY, width, height);
        this.maxWidth = 650 - width; //canvas height - player width
        this.maxHeight = 350 - height; //canvas height - player height
        this.lives = lives;
    }

    @Override
    public boolean isAttacking() {
        return isAttacking;
    }

    @Override
    public void updatePos(double dx, double dy) {
        posX += dx;
        posY += dy;
        if (posX <= 0) posX = 0;
        if (posX + width >= maxWidth) posX = maxWidth - width;
        if (posY <= 0) posY = 0;
        if (posY + height >= maxHeight) posY = maxHeight - height;
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.drawImage(state.getImage(), posX, posY);
    }


    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }


    @Override
    public Object getBoundary() {
        boundary.x = posX;
        boundary.y = posY;
        return boundary;
    }

    public void setState(Sprite.Type type) {
        if (this.state != null && this.state.getType() == type) {
            return;
        }
        this.state = spriteProvider.getPlayerState(type);
    }

    public void setAttackAnimation() {
        Thread t1 = new Thread(() -> {
            try {
                isAttacking = true;
                //Attack animation
                setState(Sprite.Type.Attack);
                Thread.sleep(500);
                //Idle state
                setState(Sprite.Type.Idle);

                Thread.sleep(ATTACK_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            isAttacking = false;
        });
        t1.start();
    }

    @Override
    public void attack(Character[] targets) {
        if (isAttacking) {
            return;
        }
        setAttackAnimation();
        for (Character character : targets) {
            character.onHit();
        }
    }

    @Override
    public int getHp() {
        return lives;
    }

    @Override
    public void onHit() {
        lives--;
    }

}
