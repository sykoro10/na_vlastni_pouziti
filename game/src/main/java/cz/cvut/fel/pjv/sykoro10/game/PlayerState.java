package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.image.Image;

import java.lang.reflect.Type;


public class PlayerState {
    private Image image;
    public PlayerState(Type type){
        setImage(type);
    }

    public void setImage(Type type){
        String path = type.resPath;
        if (path == null){
            throw new IllegalStateException("Path does not exist");
        }
        this.image = new Image(path);
    }

    public Image getImage(){
        return image;
    }

    public double getWidth(){
        return image.getWidth();
    }

    public double getHeight(){
        return image.getHeight();
    }

    public enum Type {
        Idle("/org/Colour2/Outline/120x80_gifs/__Idle.gif"),
        Attack("/org/Colour2/Outline/120x80_gifs/__Attack.gif"),
        Run("/org/Colour2/Outline/120x80_gifs/__Run.gif");

        final  String resPath;

        Type(String resPath){
            this.resPath = resPath;
        }
    }
}
